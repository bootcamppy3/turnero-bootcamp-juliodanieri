from django.db import models

class Permisos(models.Model):
    descripcion = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Permiso'
        verbose_name_plural = 'Permisos'


class Rol(models.Model):
    descripcion = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Rol'
        verbose_name_plural = 'Roles'


class Usuario(models.Model):
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255)
    user_login = models.CharField(max_length=255)
    password_login = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

class DetUserRol(models.Model):
    rol = models.ForeignKey('Rol', models.DO_NOTHING, null=True)
    usuario = models.ForeignKey('Usuario', models.DO_NOTHING, null=True)

    class Meta:
        verbose_name = 'DetUserRol'
        verbose_name_plural = 'DetUserRoles'


class ColaEspera(models.Model):
    tipo_servicio = models.ForeignKey('TipoServicio', models.DO_NOTHING, null=True)
    tickets = models.ForeignKey('Tickets', models.DO_NOTHING, null=True)

    class Meta:
        verbose_name = 'ColaEspera'
        verbose_name_plural = 'ColaEsperas'


class DetRolesPermisos(models.Model):
    rol = models.ForeignKey('Rol', models.DO_NOTHING, null=True)
    permiso = models.ForeignKey('Permisos', models.DO_NOTHING, null=True)

    class Meta:
        verbose_name = 'DetRolesPermiso'
        verbose_name_plural = 'DetRolesPermisos'

class NivelesPrioridad(models.Model):
    niveles = models.CharField(max_length=255)
    descripcion = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'NivelesPrioridad'
        verbose_name_plural = 'NivelesPrioridad'

class Estado(models.Model):
    descripcion = models.CharField(max_length=255)
    activo = (
    ('1', 'SI'),
    ('2', 'NO'),
    )
    activo = models.CharField(max_length=2, choices=activo)

    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estado'

class Tickets(models.Model):
    numero_ticket = models.IntegerField()
    descripcion = models.ForeignKey('Persona', models.CharField,  null=True, blank=True)
    estado = models.ForeignKey('Estado', models.DO_NOTHING, null=True)
    prioridades = models.ForeignKey('NivelesPrioridad', models.DO_NOTHING, null=True)
    fecha = models.DateField(blank=True, null=True)
    hora = models.TimeField(blank=True, null=True)

    class Meta:
        verbose_name = 'Tickets'
        verbose_name_plural = 'Tickets'

class TipoPersona(models.Model):
    descripcion = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'TipoPersona'
        verbose_name_plural = 'TipoPersonas'

class Persona(models.Model):
    descripcion = models.ForeignKey('TipoPersona', models.DO_NOTHING, blank=True, null=True)
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255)
    numero_documento = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'

class Puesto(models.Model):
    usuario = models.ForeignKey('Usuario', models.DO_NOTHING, null=True)
    descripcion = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Puesto'
        verbose_name_plural = 'Puestos'

class TipoServicio(models.Model):
    puesto = models.ForeignKey('Puesto', models.DO_NOTHING, null=True)
    descripcion = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'TipoServicio'
        verbose_name_plural = 'TipoServicios'


